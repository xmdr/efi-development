#!/bin/bash

# Create the output directory
if [ ! -d bin ]; then
    mkdir bin
fi

# Build EFI applications in each directory
for dir in */; do
    if [[ "$dir" != "bin/" && "$dir" != "esp_mount/" ]]; then
        dir=${dir%*/}  # Remove trailing slash

        gcc ${dir}/*.c                         \
            -c                                 \
            -fno-stack-protector               \
            -fpic                              \
            -fshort-wchar                      \
            -mno-red-zone                      \
            -I /usr/include/efi                \
            -I /usr/include/efi/x86_64         \
            -DEFI_FUNCTION_WRAPPER             \
            -o bin/${dir}.o

        if [ $? -ne 0 ]; then
            echo "Hint: install the gnu-efi package"
            exit 1;
        fi
         
        ld bin/${dir}.o                    \
            /usr/lib/crt0-efi-x86_64.o     \
            -nostdlib                      \
            -znocombreloc                  \
            -T /usr/lib/elf_x86_64_efi.lds \
            -shared                        \
            -Bsymbolic                     \
            -L /usr/lib                    \
            -l:libgnuefi.a                 \
            -l:libefi.a                    \
            -o bin/${dir}.so

        if [ $? -ne 0 ]; then
            exit 1;
        fi
         
        objcopy -j .text                \
                -j .sdata               \
                -j .data                \
                -j .dynamic             \
                -j .dynsym              \
                -j .rel                 \
                -j .rela                \
                -j .reloc               \
                --target=efi-app-x86_64 \
                bin/${dir}.so           \
                bin/${dir}.efi

        if [ $? -ne 0 ]; then
            exit 1;
        fi
    fi
done

# Check if the drive.fat file already exists
if [[ -e "esp.img" ]]; then
    echo "esp.img file already exists"
else
    echo "Creating esp.img file..."
    dd if=/dev/zero of=esp.img bs=1M count=1024
    echo "Partitioning"
    mkfs.fat -F32 esp.img
fi

# Create a mount directory
echo "Creating mount directory"
mkdir esp_mount

# Mount the EFI FAT filesystem using udisksctl
echo "Mounting - this requires sroot"
sudo mount -o loop esp.img esp_mount

if [ $? -ne 0 ]; then
    exit 1;
fi

# Copy all EFI files from the current directory to the mounted filesystem
echo "Copying efi files to image - this requires root"
sudo cp bin/*.efi esp_mount/

# Unmount the EFI FAT filesystem using udisksctl
echo "Unmounting image - this requires root"
sudo umount esp_mount

# Clean up by removing the mount directory
echo "Removing mount directory"
rmdir esp_mount
