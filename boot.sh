qemu-system-x86_64 \
-drive format=raw,file=esp.img \
-drive if=pflash,format=raw,file=OVMF.fd \
-drive if=pflash,format=raw,file=OVMF_VARS.fd \
-bios OVMF.fd
