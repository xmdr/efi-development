# EFI adventures

Little repo for me to quickly iterate on EFI applications.

## Usage

- Make a new directory for each application
- It will get compiled to bin/dirname.{o,so,efi}
- ./build.sh will create a disk image and copy all bin/*.efi to it
- ./boot.sh will boot the disk image
- Once booted, run your app with `fs0:\app.efi`
